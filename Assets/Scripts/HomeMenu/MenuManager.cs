using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class MenuManager : MonoBehaviour
{
    public void StartPlayer()
    {
        SceneManager.LoadScene("Scene_1");
    }
    public void Exit()
    {
        Application.Quit();
    }
}
