using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraController : Singleton<CameraController>
{
    private CinemachineVirtualCamera cinemachineVitrualCamera;
    private void Start()
    {
        SetPlayerCameraFollow();
    }
    public void SetPlayerCameraFollow()
    {
        cinemachineVitrualCamera = FindObjectOfType<CinemachineVirtualCamera>();
        cinemachineVitrualCamera.Follow = PlayerController.Instance.transform;
    }
}
