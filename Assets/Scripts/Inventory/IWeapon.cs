interface IWaepon
{
    public void Attack();
    public WeaponInfo GetWeaponInfo();
}