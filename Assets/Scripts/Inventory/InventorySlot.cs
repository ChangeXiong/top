using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;

public class InventorySlot : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] int slotIndex = 0;
    [SerializeField] private WeaponInfo weaponInfo;
    
    public WeaponInfo GetWeaponInfo()
    {
        return weaponInfo;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        ActiveInventory.Instance.ToggleActiveSlot(slotIndex);
    }
}
